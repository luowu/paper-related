# paper-related

## cookie security & CSRF

- (read)The Unexpected Dangers of Dynamic JavaScript(Sec 15)
- Attack Patterns for Black-Box Security Testing of Multi-Party Web Applications(NDSS 16)
- (read)The Cracked Cookie Jar: HTTP Cookie Hijacking and the Exposure of Private Information(SP 16)
- (read)Large-Scale Analysis & Detection of Authentication Cross-Site Request Forgeries(EuroSP 17)
- Deemon: Detecting CSRF with Dynamic Analysis and Property Graphs(CCS 17)
- (read)Who Left Open the Cookie Jar? A Comprehensive Evaluation of Third-Party Cookie Policies(Sec 18)

## SOP

### SOP

- Eradicating DNS Rebinding with the Extended Same-origin Policy(Sec 13)
- Language-based Defenses against Untrusted Browser Origins(Sec 13)
- (read)The “Web/Local” Boundary Is Fuzzy: A Security Study of Chrome’s Process-based Sandboxing(CCS 16)
- Rewriting History: Changing the Archived Web from the Present(CCS 17)
- (read)Same-Origin Policy: Evaluation in Modern Browsers(Sec 17)
- (read)On the Content Security Policy Violations due to the Same-Origin Policy(WWW 17)
- (read)Revisiting Browser Security in the Modern Era: New Data-only Attacks and Defenses(EuroS&P 17)
- DATS – Refactoring Access Control Out of Web Applications(ASPLOS 18)

### cross-origin isolation/communication(postMessage)

- (read)Securing Frame Communication in Browsers(Sec 08)
- (read)The Multi-Principal OS Construction of the Gazelle Web Browser(Sec 09)
- (partially read)Flax: Systematic discovery of client-side validation vulnerabilities in rich web applications(NDSS 10)
- Privilege Separation in HTML5 Applications(Sec 12)
- (read)The Postman Always Rings Twice- Attacking and Defending postMessage in HTML5 Websites(NDSS 13)
- Automating isolation and least privilege in web services(SP 14)
- (partially read)All your screens are belong to us: Attacks exploiting the html5 screen sharing api(SP 14)
- Pivot: Fast, synchronous mashup isolation using generator chains(SP 14)
- JaTE: Transparent and Efficient JavaScript Confinement(ACSAC 15)
- (read)ZigZag: Automatically Hardening Web Applications Against Client-side Validation Vulnerabilities(Sec 15)
- (read)Privacy Breach by Exploiting postMessage in HTML5: Identification, Evaluation, and Countermeasure(ASIA CCS 16)

### cross-origin objects sharing?

- OMash: Enabling Secure Web Mashups via Object Abstractions(CCS 08)
- (read)Cross-Origin JavaScript Capability Leaks: Detection, Exploitation, and Defense(Sec 09)
- (partially read)Object Views: Fine-Grained Sharing in Browsers(WWW 10)
- Towards Fine-Grained Access Control in JavaScript Contexts(ICDCS 11)
- (read)The Emperor’s New APIs- On the (In)Secure Usage of New Client-side Primitives(W2SP 10)
- (read)Redefining Web Browser Principals with a Configurable Origin Policy(DSN 13)
- Protecting Web-based Single Sign-on Protocols against Relying Party Impersonation Attacks through a Dedicated Bi-directional Authenticated Secure Channel(RAID 14)

## JS

- You Are What You Include: Large-scale Evaluation of Remote JavaScript Inclusions(CCS 12)
- Rozzle: De-cloaking internet malware(SP 12)
- (read)Protecting users by confining JavaScript with COWL(OSDI 14)
- Inlined Information Flow Monitoring for JavaScript(CCS 15)
- FlowWatcher: Defending against data disclosure vulnerabilities in web applications(CCS 15)
- (partially read)Understanding and Monitoring Embedded Web Scripts(S&P 15)
- Finding and Preventing Bugs in JavaScript Bindings(S&P 17)
- Enabling Reconstruction of Attacks on Users via Efficient Browsing Snapshots(NDSS 17)
- Thou shalt not depend on me: Analysing the use of outdated javascript libraries on the web(NDSS 17)
- (partially read)J-Force: Forced Execution on JavaScript(WWW 17)
- JSgraph: Enabling Reconstruction of Web Attacks via Efficient Tracking of Live In-Browser JavaScript Executions(NDSS 18)
- JavaScript Zero: Real JavaScript and Zero Side-Channel Attacks(NDSS 18)
- Putting in all the stops: execution control for JavaScript(PLDI 18)

## XSS

- Document Structure Integrity: A Robust Basis for Cross-site Scripting Defense(NDSS 09)
- 25 million flows later: Large-scale detection of DOM-based XSS(CCS 13)
- eDacota: toward preventing server-side XSS via automatic code and data separation(CCS 13)
- Precise Client-side Protection against DOM-based Cross-Site Scripting(CCS 14)
- Hunting the red fox online: Understanding and detection of mass redirect-script injections(SP 14)
- From Facepalm to Brain Bender: Exploring Client-Side Cross-Site Scripting(CCS 15)
- The SICILIAN defense: Signature-based whitelisting of web JavaScript(CCS 15)
- Code-Reuse Attacks for the Web: Breaking Cross-Site Scripting Mitigations via Script Gadgets(CCS 17)
- Synode: Understanding and Automatically Preventing Injection Attacks on Node.js(NDSS 18)
- Riding out DOMsday: Towards Detecting and Preventing DOM Cross-Site Scripting(NDSS 18)

### AD

- AdJail: Practical Enforcement of Confidentiality and Integrity Policies on Web Advertisements(Sec 10)
- AdSentry: comprehensive and flexible confinement of JavaScript-based advertisements(ACSAC 11)
- Knowing your enemy: understanding and detecting malicious web advertising(CCS 12)
- Shady paths: Leveraging surfing crowds to detect malicious web pages(CCS 13)
- Ad injection at scale: Assessing deceptive advertisement modifications(SP 15)
- Unraveling the relationship between ad-injecting browser extensions and malvertising(WWW 15)

## extension

- (read)Hulk: Eliciting Malicious Behavior in Browser Extensions(Sec 14)
- CrossFire: An Analysis of Firefox Extension-Reuse Vulnerabilities(NDSS 16)
- (read)Extension Breakdown: Security Analysis of Browsers Extension Resources Control Policies(Sec 17)
- Mystique: Uncovering Information Leakage from Browser Extensions(CCS 18)

## JIT

- Librando: transparent code randomization for just-in-time compilers(CCS 13)
- Just-in-time code reuse: On the effectiveness of fine-grained address space layout randomization(SP 13)
- RockJIT: Securing just-in-time compilation using modular control-flow integrity(CCS 14)
- JITScope: Protecting web users from control-flow hijacking attacks(INFOCOM 15)
- Exploiting and protecting dynamic code generation(NDSS 15)
- (read)The Devil is in the Constants: Bypassing Defenses in Browser JIT Engines(NDSS 15)
- What Cannot Be Read, Cannot Be Leveraged? Revisiting Assumptions of JIT-ROP Defenses(Sec 16)
- Return to the Zombie Gadgets: Undermining Destructive Code Reads via Code Inference Attacks(SP 16)
- (read)JITGuard: Hardening Just-in-time Compilers with SGX(CCS 17)
- (read)Dachshund: Digging for and Securing (Non-)Blinded Constants in JIT Code(NDSS 17)

## user tracking

### third-party user tracking

- Detecting and Defending Against Third-Party Tracking on the Web(NSDI 12)
- Third-party web tracking: Policy and technology(S&P 12)
- Your online interests: Pwned! a pollution attack against targeted advertising(CCS 14)
- I Do Not Know What You Visited Last Summer:Protecting Users from Third-party Web Tracking with TrackingFree Browser(NDSS 15)
- Internet Jones and the Raiders of the Lost Trackers: An Archaeological Study of Web Tracking from 1996 to 2016(Sec 16)
- Tracing Information Flows Between Ad Exchanges Using Retargeted Ads(Sec 16)
- Online tracking: A 1-million-site measurement and analysis(CCS 16)
- Block me if you can: A large-scale study of tracker-blocking tools(EuroS&P 17)
- Web Tracking Technologies and Protection Mechanisms(CCS 17)

### fingerprint/cookieless user tracking

- Cookieless Monster: Exploring the Ecosystem of Web-Based Device Fingerprinting(S&P 13)
- The web never forgets: Persistent tracking mechanisms in the wild(CCS 14)
- Beauty and the Beast: Diverting modern web browsers to build unique browser fingerprints(S&P 16)
- k-fingerprinting: A Robust Scalable Website Fingerprinting Technique(Sec 16)
- Website Fingerprinting at Internet Scale(NDSS 16)
- XHOUND: Quantifying the Fingerprintability of Browser Extensions(S&P 17)
- Walkie-Talkie: An Efficient Defense Against Passive Website Fingerprinting Attacks(Sec 17)
- (Cross-)Browser Fingerprinting via OS and Hardware Level Features(NDSS 17)
- FP-STALKER: Tracking Browser Fingerprint Evolutions Along Time(S&P 18)
- (read)Fp-Scanner: The Privacy Implications of Browser Fingerprint Inconsistencies(Sec 18)
- Clock Around the Clock: Time-Based Device Fingerprinting(CCS 18)

## CORS

- Request and Conquer: Exposing Cross-Origin Resource Size(Sec 17)
- (read)We Still Don’t Have Secure Cross-Domain Requests: an Empirical Study of CORS(Sec 18)

## CSP

- Cspautogen: Black-box enforcement of content security policy upon real-world websites(CCS 16)
- Content security problems?: Evaluating the effectiveness of content security policy in the wild(CCS 16)
- CSP is dead, long live CSP! On the insecurity of whitelists and the future of content security policy(CCS 16)
- CCSP: Controlled relaxation of content security policies by runtime policy composition(Sec 17)

## private mode

- UCognito: Private Browsing without Tears(CCS 15)
- Your Secrets Are Safe: How Browsers' Explanations Impact Misconceptions About Private Browsing Mode(WWW 18)
- Veil: Private Browsing Semantics Without Browser-side Assistance(NDSS 18)

## side-channel

- Cross-Site Search Attacks(CCS 15)
- Identifying Cross-origin Resource Status Using Application Cache(NDSS 15)
- The clock is still ticking: Timing attacks in the modern web(CCS 16)
- Dedup est machina: Memory deduplication as an advanced exploitation vector(SP 16)
- Loophole: Timing attacks on shared event loops in chrome(Sec 17)

## others

- Towards Measuring and Mitigating Social Engineering Software Download Attacks.(Sec 16)
- Don't Let One Rotten Apple Spoil the Whole Barrel: Towards Automated Detection of Shadowed Domains(CCS 17)
- The wolf of name street: Hijacking domains through their nameservers(CCS 17)
- Game of Registrars: An Empirical Analysis of Post-Expiration Domain Name Takeovers(Sec 17)
- Most Websites Don’t Need to Vibrate: A Cost-Benefit Approach to Improving Browser Security(CCS 17)

## Integrity

- Securing Web Applications with Secure Coding Practices and Integrity Verification

## Android

- (read)Breaking and Fixing Origin-Based Access Control in Hybrid Web/Mobile Application Frameworks(NDSS 14)
- Draco: A system for uniform and fine-grained access control for web code on android(CCS 16)
- What Mobile Ads Know About Mobile Users (NDSS 16)
- Secure integration of web content and applications on commodity mobile operating systems(CCS 17)
- HybridGuard: A Principal-based Permission and Fine-Grained Policy Enforcement Framework for Web-based Mobile Applications(SP 17)
- (read)Study and Mitigation of Origin Stripping Vulnerabilities in Hybrid-postMessage Enabled Mobile Applications(SP 18)
- An Empirical Study of Web Resource Manipulation in Real-world Mobile Applications(Sec 18)

## XoR

- Norax: Enabling Execute-Only Memory for COTS Binaries on AArch64(S&P 17)

## sandbox

- Ryoan: A Distributed Sandbox for Untrusted Computation on Secret Data(OSDI 16)

 
